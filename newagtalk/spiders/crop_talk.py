# -*- coding: utf-8 -*-

import re
from datetime import datetime

from bs4 import BeautifulSoup as bs

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.http.request import Request
from scrapy.selector import Selector

from newagtalk.items import TopicItem, MessageItem


class CropTalkSpider(CrawlSpider):
    name = "crop_talk"
    allowed_domains = ["talk.newagtalk.com"]
    start_urls = (
        'http://talk.newagtalk.com/forums/forum-view.asp?fid=3',
    )

    rules = (
        Rule(LinkExtractor(allow=(r'/forums/forum-view.asp\?fid=3&bookmark=\d+&displaytype=threaded')), follow=True),
        Rule(LinkExtractor(allow=(r'/forums/thread-view.asp\?tid=\d+&mid=\d+#M\d+')), callback='parse_message'),
    )

    # def parse(self, response):
    #
    #     from scrapy.shell import inspect_response
    #     inspect_response(response)
    #
    #     pass

    def parse_message(self, response):

        # from scrapy.shell import inspect_response
        # inspect_response(response)

        # http://talk.newagtalk.com/forums/thread-view.asp?mid=1644548&tid=217889

        post_url = response.url
        url_patten = re.compile(r'http://talk.newagtalk.com/forums/thread-view\.asp\?mid=(\d+)&tid=(\d+)')
        url_re_res = re.match(url_patten, post_url).groups()
        post_mid = int(url_re_res[0])
        post_tid = int(url_re_res[1])

        mhs = response.xpath('//td[@class="messageheader"]')
        user_name = mhs[0].xpath('./a/text()').extract()[0]

        post_info_span = mhs[1].xpath('.//span[@class="smalltext"]').extract()[0]
        post_info_text = bs(post_info_span).text
        p = re.compile(r'Posted (\d+)/(\d+)/(\d+)  (\d+):(\d+) \(#(.+)\) Subject: (.*)')
        re_res = re.match(p, post_info_text).groups()

        is_topic = False
        post_title = ''
        if 'in reply to' not in re_res[5]:
            is_topic = True
            post_title = re_res[6]


        post_month = int(re_res[0])
        post_day = int(re_res[1])
        post_year = int(re_res[2])
        post_hour = int(re_res[3])
        post_min = int(re_res[4])
        post_datetime = datetime(year=post_year, month=post_month, day=post_day, hour=post_hour, minute=post_min)

        mms = response.xpath('//td[@class="messagemiddle"]')

        user_info_span = mms[0].xpath('.//span[@class="smalltext"]').extract()[0]
        user_location = bs(user_info_span).text.strip()

        post_content_td = mms[1].extract()
        post_content = bs(post_content_td).text.strip()

        if is_topic:
            item = TopicItem()
            item['tid'] = post_tid
            item['title'] = post_title
            item['posted_at'] = post_datetime
            item['user_name'] = user_name
            item['user_location'] = user_location
            yield item

        item = MessageItem()
        item['mid'] = post_mid
        item['tid'] = post_tid
        item['posted_at'] = post_datetime
        item['user_name'] = user_name
        item['user_location'] = user_location
        item['content'] = post_content
        yield item






